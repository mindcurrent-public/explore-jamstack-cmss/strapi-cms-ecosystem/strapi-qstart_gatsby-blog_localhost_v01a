# strapi-qstart_gatsby-blog_localhost_v01a

We will follow the **[Strapi QuickStart Guide](https://strapi.io/documentation/developer-docs/latest/getting-started/quick-start.html)** to create a Gatsby-based blog running on localhost.

This turned out not to be as straight-forward as it initially seemed, and actually raised a software QA/QC issue:
* [Resolve Gatsby Blog site problem (images not rendering) submitted to the Strapi Member Forum (per responses)](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-qstart_gatsby-blog_localhost_v01a/-/issues/4)   
...that was resolved by the helpful Strapi support team.

Details of the saga are tracked in the issues, in a _"micro-task"_ GSD manner.

---

### Approach:
* As we proceed, we'll [track our progress on a per-issue basis](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-qstart_gatsby-blog_localhost_v01a/-/issues), in a "GSD using micro-tasks" manner.   
* Useful SOPs can be constructed from the project history tracked via the [closed issues](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-qstart_gatsby-blog_localhost_v01a/-/issues?scope=all&state=closed).

### Results:
* Strapi CMS server (on localhost): http://localhost:1337/admin/   
  * Strapi REST API Example Endpoints:  
    * http://localhost:1337/articles/beautiful-picture
    * http://localhost:1337/articles/how-we-troubleshot-our-strapi-based-gatsby-blog
  * Strapi GraphQL Explorer:  http://localhost:1337/graphql
* Gatsby Static Site (on localhost): http://localhost:8000/

### PaaS Management Dashboards:
* Cloudinary Media Management PaaS: (via a [personal account](https://cloudinary.com/console/c-cb82244776fa22dd910eb8f1488974/media_library/folders/home))


### Dynalist Action-Items
* Strapi Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=7ujn_CDYtGZ-xMse1r3VqIKe))
* Gatsby Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=u-B_PD2Nlr0KahscXewpfW5E))

